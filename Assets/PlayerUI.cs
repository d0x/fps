﻿using UnityEngine;

public class PlayerUI : MonoBehaviour {

	[SerializeField]
	private RectTransform thrusterFuelFill;

	private PlayerController controller;

	void SetFuelAmount(float amount) {
		//Debug.Log (amount);
		thrusterFuelFill.localScale = new Vector3 (1f, amount, 1f);
	}

	public void SetController(PlayerController controller){
		this.controller = controller;
	}
		
	void Start () {
	
	}

	void Update () {
		SetFuelAmount (controller.GetThrusterFuelAmont ()); 
	}
}
