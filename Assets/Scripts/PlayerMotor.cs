﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour {

	[SerializeField]
	private Camera cam;

	[SerializeField]
	private float cameraRotationLimit = 85f;

	private Vector3 velocity = Vector3.zero;
	private Vector3 rotation  = Vector3.zero;

	private float cameraRotationX  = 0f;
	private float currentCameraRotationX  = 0f;

	private Vector3 thrusterForce= Vector3.zero;


	private Rigidbody rb;

	void Start() {
		rb = GetComponent<Rigidbody> ();
	}
		
	public void Move (Vector3 velocity)
	{
		this.velocity = velocity;
	}

	public void Rotate (Vector3 rotation)
	{
		this.rotation = rotation;
	}

	public void RotateCamera (float cameraRotationX)
	{
		this.cameraRotationX = cameraRotationX;
	}

	public void ApplyThruster (Vector3 thrusterForce)
	{
		this.thrusterForce = thrusterForce;
	}

	// Run every physics iteration
	void FixedUpdate()
	{
		PerformMovemnt ();
		PerformRotations ();
	}

	void PerformMovemnt ()
	{
		if (velocity != Vector3.zero) {
			rb.MovePosition (rb.position + velocity * Time.fixedDeltaTime);
		}

		if (thrusterForce != Vector3.zero) {
			rb.AddForce (thrusterForce * Time.deltaTime, ForceMode.Acceleration);
		}
	}
		
	void PerformRotations ()
	{
		rb.MoveRotation (rb.rotation * Quaternion.Euler(rotation));
		if (cam != null) {
			// set our rotation and clamp it.
			currentCameraRotationX -= cameraRotationX;
			currentCameraRotationX = Mathf.Clamp (currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

			// apply our rotation to the transform of our camera
			cam.transform.localEulerAngles = new Vector3 (currentCameraRotationX, 0f, 0f);
		}
	}
}
