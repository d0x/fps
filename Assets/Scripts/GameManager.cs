﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public static GameManager instance;

	[SerializeField]
	private GameObject sceneCamera;

	public MatchSettings matchSettings;

	void Awake() {

		if (instance != null) {
			Debug.Log ("More then one game manager in scene!");
		} else {
			instance = this;
		}
	}

	public void SetSceneCameraActive(bool isActive){ 
		if (sceneCamera == null) {
			return;
		}

		sceneCamera.SetActive (isActive);
	}

	#region Player tracking

	private const string PLAYER_PREFIX = "Player ";

	private static Dictionary<string, Player> players = new Dictionary<string, Player>();

	public static void RegisterPlayer(string netID, Player player)
	{
		string playerId = PLAYER_PREFIX + netID;

		players.Add (playerId, player);

		player.transform.name = playerId;
	}

	public static void UnRegister (string playerId)
	{
		players.Remove (playerId);
	}

//	void OnGUI()
//	{
//		GUILayout.BeginArea (new Rect (200, 200, 200, 500));
//		GUILayout.BeginVertical ();
//
//		foreach(string playerId in players.Keys)
//		{
//			GUILayout.Label (playerId + " - " + players [playerId].transform.name);
//		}
//
//		GUILayout.EndVertical ();
//		GUILayout.EndArea ();
//	}	

	public static Player GetPlayer(string playerId){
		return players [playerId];
	}

	#endregion 
}
