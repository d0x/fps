﻿using UnityEngine;
using UnityEngine.Networking; 

public class WeaponManager : NetworkBehaviour {

	[SerializeField]
	private string weaponLayerName = "Weapon";

	[SerializeField]
	private PlayerWeapon primaryWeapon;

	[SerializeField]
	private Transform weaponHoler;

	private PlayerWeapon currentWeapon;
	private WeaponGraphics currentGraphics;


	void Start() {
		EquipWeapon (primaryWeapon);
	}

	void EquipWeapon(PlayerWeapon playerWeapon){
		this.currentWeapon = playerWeapon;

		GameObject weaponIns = (GameObject) Instantiate (currentWeapon.graphics, weaponHoler.position, weaponHoler.rotation);
		weaponIns.transform.SetParent (weaponHoler);

		currentGraphics = weaponIns.GetComponent<WeaponGraphics> ();

		if (currentGraphics == null) {
			Debug.LogError ("No Weapongraphics component on the weapon object: " + weaponIns.name);
		}
			
		if (isLocalPlayer) {
			Util.SetLayerRecursivly(weaponIns, LayerMask.NameToLayer(weaponLayerName));
		}

	}

	public PlayerWeapon getCurrentWeapon(){
		return currentWeapon;
	}

	public WeaponGraphics getCurrentGraphics(){
		return currentGraphics;
	}
}
