﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(PlayerController))]
public class PlayerSetup : NetworkBehaviour {

	[SerializeField]
	Behaviour[] componentsToDisable;

	[SerializeField]
	string remoteLayerName = "RemotePlayer";

	[SerializeField]
	string dontDrawLayerName = "DontDraw";

	[SerializeField]
	GameObject playerGraphics;

	[SerializeField]
	private GameObject playerUIPrefab;
	  
	[HideInInspector]
	public GameObject playerUIInstance;

	void Start()
	{
		if (!isLocalPlayer) {
			DisableComponents ();
			AssignRemoteLayer ();
		}
		else {

			// Disable player graphics for localPlayer
			SetLayerRecursively(playerGraphics, LayerMask.NameToLayer(dontDrawLayerName));

			// Create Plazyer UI
			playerUIInstance = Instantiate(playerUIPrefab);
			playerUIInstance.name = playerUIPrefab.name;

			PlayerUI playerUI = playerUIInstance.GetComponent<PlayerUI> ();
			if (playerUI == null) {
				Debug.LogError ("No PlayerUi component on playerui prefab");
			} else {
				playerUI.SetController (GetComponent<PlayerController> ());
			}

			GetComponent<Player> ().SetupPlayer ();
		}
	}

	void SetLayerRecursively (GameObject obj, int newLayer)
	{
		obj.layer = newLayer;

		foreach (Transform child in obj.transform) {
			SetLayerRecursively (child.gameObject, newLayer);
		}
	}

	public override void OnStartClient() {
		base.OnStartClient ();

		string netId = GetComponent<NetworkIdentity> ().netId.ToString();
		Player player = GetComponent<Player> ();

		GameManager.RegisterPlayer (netId, player);
	}

	void AssignRemoteLayer(){
		gameObject.layer = LayerMask.NameToLayer(remoteLayerName);
	}

	void DisableComponents ()
	{
		for (int i = 0; i < componentsToDisable.Length; i++) {
			componentsToDisable [i].enabled = false;
		}
	}

	void OnDisable() {
		Destroy (playerUIInstance);

		if (isLocalPlayer) {
			GameManager.instance.SetSceneCameraActive (true);
			GameManager.UnRegister (transform.name);
		}
	}

}
