﻿	using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(ConfigurableJoint))]
[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour {
	
	[SerializeField]
	private float speed = 5f;

	[SerializeField]
	private float lookSensitivity = 3f;

	[SerializeField]
	private LayerMask enviromentMask;

	[Header("Thruster Settings")]

	[SerializeField]
	private float thrusterForce = 1300f;

	[SerializeField]
	private float thrusterFuelBurnSpeed = 1f;

	[SerializeField]
	private float thrusterFuelRegenSpeed = 0.3f;

	private float thrusterFuelAmount = 1f;

	[Header("Spring Settings:")]

	[SerializeField]
	private float jointSpring = 20f;

	[SerializeField]
	private float jointMaxForce = 40f;

	// Component caching
	private  Animator animator;
	private PlayerMotor motor;
	private ConfigurableJoint joint;

	void Start(){
		motor = GetComponent<PlayerMotor> ();
		joint = GetComponent<ConfigurableJoint> ();
		animator = GetComponent<Animator> ();

		SetJointSettings (jointSpring);
	}

	public float GetThrusterFuelAmont(){ 
		return thrusterFuelAmount;
	}

	void Update() {

		// Set Target Position for Spring. This makes the phiysics add right when it comes to applying gravity when flying over objects.
		RaycastHit hit;
		if (Physics.Raycast (transform.position, Vector3.down, out hit, 100f, enviromentMask)) {
			joint.targetPosition = new Vector3 (0f, -hit.point.y, 0f);
		} else {
			joint.targetPosition = Vector3.zero;
		}

		// Calculate movement velocity, a 3d vector
		float xMove = Input.GetAxis("Horizontal");
		float zMove = Input.GetAxis("Vertical");

		Vector3 moveHorizontal = transform.right * xMove;
		Vector3 moveVertrical = transform.forward * zMove;

		Vector3 velocity = (moveHorizontal + moveVertrical) * speed;

		// animator
		animator.SetFloat("Blend", zMove);

		motor.Move (velocity);

		// turning around
		float yRot = Input.GetAxisRaw ("Mouse X");
		Vector3 rotation = new Vector3 (0f, yRot, 0f) * lookSensitivity;
		motor.Rotate (rotation);

		// tilt camera
		float xRot = Input.GetAxisRaw ("Mouse Y");
		float cameraRotationX = xRot * lookSensitivity;
		motor.RotateCamera (cameraRotationX);

		// Calc thrusterforce based on player input
		Vector3 thrusterForce = Vector3.zero;
		if (Input.GetButton ("Jump") && thrusterFuelAmount > 0f) {
			thrusterFuelAmount -= thrusterFuelBurnSpeed * Time.deltaTime;

			if (thrusterFuelAmount > 0.01f) {
				thrusterForce = Vector3.up * this.thrusterForce;
				SetJointSettings (0f);
			}
		} else {
			thrusterFuelAmount += thrusterFuelRegenSpeed * Time.deltaTime;
			SetJointSettings (jointSpring);
		}

		thrusterFuelAmount = Mathf.Clamp (thrusterFuelAmount, 0f, 1f);

		motor.ApplyThruster (thrusterForce);
	}

	private void SetJointSettings(float jointSpring){
		joint.yDrive = new JointDrive{ 
			maximumForce = jointMaxForce,
			positionSpring = jointSpring
		};
	}
}
