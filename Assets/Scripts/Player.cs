﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent(typeof(PlayerSetup))]
public class Player : NetworkBehaviour{

	[SyncVar]
	private bool dead = false;

	public bool isDead
	{
		get { return dead; }	
		protected set { dead = value; }
	}

	[SerializeField]
	private int maxHealth = 100;

	[SyncVar]
	private int currentHealth;

	[SerializeField]
	private GameObject[] disableGameObjectOnDeath;

	[SerializeField]
	private Behaviour[] disableOnDeath;
	private bool[] wasEnabled;

	[SerializeField]
	private GameObject deathEffect;

	[SerializeField]
	private GameObject spawnEffect;


	private bool firstSetup = true;

	public void SetupPlayer () {
		if (isLocalPlayer) {
			// switch cameras
			GetComponent<PlayerSetup> ().playerUIInstance.SetActive (true);
			GameManager.instance.SetSceneCameraActive (false);
		}

		CmdBroadcastNewPlayerSetup ();
	}

	[Command]
	private void CmdBroadcastNewPlayerSetup() {
		RpcSetupPlayerOnClients ();
	}

	[ClientRpc]
	private void RpcSetupPlayerOnClients() {

		if (firstSetup) {
			wasEnabled = new bool[disableOnDeath.Length];

			for (int i = 0; i < disableOnDeath.Length; i++) {
				wasEnabled [i] = disableOnDeath [i].enabled;
			}

			firstSetup = false;
		}

		SetDefaults ();
	}
		
	void Update(){
		if (!isLocalPlayer) {
			return;
		}

		if (Input.GetKeyDown (KeyCode.Q)) {
			RpcTakeDame (999999);
		}
	}

	[ClientRpc]
	public void RpcTakeDame (int damage)
	{
		if (isDead) {
			return;
		}

		currentHealth -= damage;

		if(currentHealth<=0)
		{
			Die();
		}

		Collider collider = GetComponent<Collider> ();
		if (collider != null) {
			collider.enabled = true;
		}
	}

	void Die ()
	{
		isDead = true;

		// DISABLE COMPONENTS
		for (int i = 0; i < disableOnDeath.Length; i++) {
			disableOnDeath [i].enabled = false;
		}

		// DISABLE GameObjects
		for (int i = 0; i < disableGameObjectOnDeath.Length; i++) {
			disableGameObjectOnDeath [i].SetActive(false);
		}

		// disable collider
		Collider collider = GetComponent<Collider> ();
		if (collider != null) {
			collider.enabled = false;
		}

		//spawn death effect
		GameObject gfxInstance = (GameObject) Instantiate(deathEffect,transform.position, Quaternion.identity);
		Destroy (gfxInstance, 3f);

		// switch cameras
		if (isLocalPlayer) {
			GameManager.instance.SetSceneCameraActive (true);
			GetComponent<PlayerSetup> ().playerUIInstance.SetActive (false);
		}

		Debug.Log(transform.name + " is dead!");

		// call respawn method
		StartCoroutine (Respawn());

	}

	private IEnumerator Respawn()
	{
		yield return new WaitForSeconds (GameManager.instance.matchSettings.respawnTime);

		Transform spawnPoint = NetworkManager.singleton.GetStartPosition ();

		transform.position = spawnPoint.position;
		transform.rotation = spawnPoint.rotation;

		yield return new WaitForSeconds (0.1f);

		SetupPlayer ();

		Debug.Log (transform.name + " respawned");
	}

	void SetDefaults ()
	{
		isDead = false;
		currentHealth = maxHealth;

		// Enable Compoents
		for (int i = 0; i < disableOnDeath.Length; i++) {
			disableOnDeath [i].enabled = wasEnabled [i];
		}

		// Enable GameObjects
		for (int i = 0; i < disableGameObjectOnDeath.Length; i++) {
			disableGameObjectOnDeath [i].SetActive(true);
		}

		// Enable Collider
		Collider col = GetComponent<Collider>();
		if (col != null)
			col.enabled = true;

		// create spawn effect
		GameObject gfxInstance = (GameObject) Instantiate(spawnEffect ,transform.position, Quaternion.identity);
		Destroy (gfxInstance, 3f);
	}

}
