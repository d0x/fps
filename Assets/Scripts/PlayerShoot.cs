﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(WeaponManager))]
public class PlayerShoot : NetworkBehaviour {

	public const string PLAYER_TAG = "Player";

	private WeaponManager weaponManager;
	private PlayerWeapon currentWeapon;

	[SerializeField]
	private Camera cam;

	[SerializeField]
	private LayerMask mask;

	void Start() {
		weaponManager = GetComponent<WeaponManager> ();

		if (cam == null) {
			Debug.LogError ("PlayerShoot: No camera reference.");
			this.enabled = false;
		}
	}

	void Update() {
		currentWeapon = weaponManager.getCurrentWeapon();

		if (currentWeapon.fireRate <= 0f) {
			if (Input.GetButtonDown ("Fire1")) {
				Shoot ();
			}
		} else {
			if(Input.GetButtonDown("Fire1"))	 {
				InvokeRepeating("Shoot", 0f, 1f/currentWeapon.fireRate);
			} else if(Input.GetButtonUp("Fire1")) {
				CancelInvoke("Shoot");
			}
		}
	}

	// Is call on the server when a player shoots
	[Command]
	void CmdOnShoot() {
		RpcDoShootEffect ();
	}


	// is called on all clients when we need to do a shoot effect.
	[ClientRpc]
	void RpcDoShootEffect() {
		weaponManager.getCurrentGraphics ().muzzleFlash.Play ();
	}

	// is called on the server when we hit something
	// takes in the hit point and normal of the surf
	[Command]
	void CmdOnHit(Vector3 pos, Vector3 normal) {
		RpcOnHitEffect (pos, normal);

	}

	[ClientRpc]
	void RpcOnHitEffect(Vector3 pos, Vector3 normal) {
		// better use object pooling
		GameObject hitEffect = (GameObject) Instantiate (weaponManager.getCurrentGraphics ().hitEffectPrefab, pos, Quaternion.LookRotation (normal));
		Destroy (hitEffect, 2f);
	}

	[Client] 
	void Shoot ()
	{
		if (!isLocalPlayer) {
			return;
		}

		// We are shoot, call the onShootMethod on the server
		CmdOnShoot ();
	
		RaycastHit hit;

		if (Physics.Raycast (cam.transform.position, cam.transform.forward, out hit, currentWeapon.range, mask)) {
			
			if (hit.collider.tag == PLAYER_TAG) {
				CmdPlayerShot (hit.collider.name, currentWeapon.damage);
			}

			// We hit something call the onhit on the server
			CmdOnHit (hit.point, hit.normal);
		}
	}


	[Command]
	void CmdPlayerShot(string playerID, int damage) 
	{
		Debug.Log ("ID " + playerID 	+ " has been shot!");

		Player player = GameManager.GetPlayer (playerID);

		player.RpcTakeDame (damage);
	}
}
