﻿using UnityEngine;
using System.Collections;

public class Util  {

	public static void SetLayerRecursivly(GameObject obj, int newLayer) {
		if (obj == null) {
			return;
		}

		obj.layer = newLayer;

		foreach (Transform child in obj.transform) {
			if (child != null) {
				SetLayerRecursivly (child.gameObject, newLayer);
			}
		}
	}

}
